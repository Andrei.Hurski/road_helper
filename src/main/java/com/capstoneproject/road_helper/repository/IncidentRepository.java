package com.capstoneproject.road_helper.repository;

import com.capstoneproject.road_helper.domain.Incident;
import com.capstoneproject.road_helper.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IncidentRepository extends JpaRepository<Incident, Long> {

    List<Incident> getByUser(User user);

    List<Incident> findAllByInOrder(boolean inOrder);

    List<Incident> findAllByUserAndInOrder(User user, Boolean inOrder);
}

