package com.capstoneproject.road_helper.domain;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = {"id"})
@EqualsAndHashCode(exclude = {"id"}, callSuper = false)
@Builder
@Entity
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "DATE")
    private LocalDateTime date;
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "VENDOR_ID", unique = true, nullable = false, updatable = false)
    private User vendor;
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "INCIDENT_ID", unique = true, nullable = false, updatable = false)
    private Incident incident;
    @Column(name = "PRICE")
    private BigDecimal price;
    @Column(name = "STATUS")
    @Enumerated(EnumType.STRING)
    private OrderStatus orderStatus;
}