package com.capstoneproject.road_helper.service;

import com.capstoneproject.road_helper.domain.Role;
import com.capstoneproject.road_helper.domain.RoleName;
import com.capstoneproject.road_helper.domain.User;
import com.capstoneproject.road_helper.repository.UserRepository;
import com.capstoneproject.road_helper.service.impl.UserServiceImpl;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
class UserServiceTest {
    @InjectMocks
    private UserServiceImpl userService;
    @Mock
    private UserRepository repository;
    private static final Long ID = 1L;
    private static final Role USER_ROLE = new Role(2, RoleName.USER);
    private static final User USER_AFTER_SAVE = new User(1L, "John", "john@email.com", "johnPassword", "+48777888999", USER_ROLE);
    private static final User USER_BEFORE_SAVE = User.builder()
            .name("John")
            .email("john@email.com")
            .password("johnPassword")
            .phone("+48777888999")
            .role(USER_ROLE)
            .build();

    private static final String EMAIL = "john@email.com";

//    @Test
//    void saveUser_shouldReturnUserWithId() {
//        when(repository.save(USER_BEFORE_SAVE)).thenReturn(USER_AFTER_SAVE);
//        User actual = userService.save(USER_BEFORE_SAVE);
//        Assert.assertNotNull(actual);
//        Assert.assertEquals(USER_AFTER_SAVE, actual);
//    }

//    @Test
//    void saveUser_shouldCallRepository() {
//        final User user = mock(User.class);
//        userService.save(user);
//        verify(repository).save(user);
//    }

    @Test
    void findById_shouldReturnOptional() {
        when(repository.findById(USER_AFTER_SAVE.getId())).thenReturn(Optional.of(USER_AFTER_SAVE));
        User actual = userService.findById(ID);
        Assert.assertNotNull(actual);
        Assert.assertEquals(USER_AFTER_SAVE, actual);
    }

    @Test
    void findById_shouldCallRepository() {
        userService.findById(ID);
        verify(repository).findById(ID);
    }

    @Test
    void findByEmail_shouldReturnOptional() {
        when(repository.findByEmail(EMAIL)).thenReturn(Optional.of(USER_AFTER_SAVE));
        User actual = userService.findByEmail(EMAIL).get();
        Assert.assertNotNull(actual);
        Assert.assertEquals(USER_AFTER_SAVE, actual);
    }

    @Test
    void findByEmail_shouldCallRepository() {
        userService.findByEmail(EMAIL);
        verify(repository).findByEmail(EMAIL);
    }

    @Test
    void findAllByRole_shouldReturnListOfUser() {
        when(repository.findAllByRole(USER_ROLE)).thenReturn(List.of(USER_AFTER_SAVE));
        List<User> actualList = userService.findAllByRole(USER_ROLE);
        User[] expected = {USER_AFTER_SAVE};
        Assert.assertNotNull(actualList);
        Assert.assertArrayEquals(expected, actualList.toArray());
        Assert.assertEquals(USER_AFTER_SAVE, actualList.get(0));
    }

    @Test
    void findAllByRole_shouldCallRepository() {
        userService.findAllByRole(USER_ROLE);
        verify(repository).findAllByRole(USER_ROLE);
    }
}