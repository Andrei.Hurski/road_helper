package com.capstoneproject.road_helper.dto;

import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AuthUserDto {
    @Email
    @NotEmpty(message = "Email cannot be empty")
    private String email;

    private String password;
}
