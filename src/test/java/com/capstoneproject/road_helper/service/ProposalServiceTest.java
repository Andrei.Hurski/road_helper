package com.capstoneproject.road_helper.service;

import com.capstoneproject.road_helper.domain.Incident;
import com.capstoneproject.road_helper.domain.Proposal;
import com.capstoneproject.road_helper.repository.ProposalRepository;
import com.capstoneproject.road_helper.service.impl.ProposalServiceImpl;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
class ProposalServiceTest {
    @InjectMocks
    private ProposalServiceImpl proposalService;
    @Mock
    private ProposalRepository repository;
    private static final Proposal PROPOSAL = new Proposal(1L, null, null, null);

    @Test
    void saveProposal_shouldReturnProposalWithId() {
        final Proposal proposal = mock(Proposal.class);
        when(repository.save(proposal)).thenReturn(PROPOSAL);
        Proposal actual = proposalService.save(proposal);
        Assert.assertNotNull(actual);
        Assert.assertEquals(Long.valueOf(1), actual.getId());
    }

    @Test
    void saveProposal_shouldCallRepository() {
        final Proposal proposal = mock(Proposal.class);
        proposalService.save(proposal);
        verify(repository).save(proposal);
    }

    @Test
    void getById_shouldReturnOptionalWithProposal() {
        when(repository.findById(1L)).thenReturn(Optional.of(PROPOSAL));
        Optional<Proposal> actualOptional = proposalService.getById(1L);
        Assert.assertEquals(Optional.of(PROPOSAL), actualOptional);
    }

    @Test
    void getById_shouldCallRepository() {
        proposalService.getById(1L);
        verify(repository).findById(1L);
    }

    @Test
    void getByIncident_shouldReturnProposalList() {
        Incident incident = mock(Incident.class);
        when(repository.findAllByIncident(incident)).thenReturn(List.of(PROPOSAL));
        List<Proposal> actualList = proposalService.getByIncident(incident);
        Assert.assertNotNull(actualList);
        Assert.assertEquals(List.of(PROPOSAL), actualList);
    }

    @Test
    void getByIncident_shouldCallRepository() {
        Incident incident = mock(Incident.class);
        proposalService.getByIncident(incident);
        verify(repository).findAllByIncident(incident);
    }
}