package com.capstoneproject.road_helper.domain;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = {"id"})
@EqualsAndHashCode(exclude = {"id"}, callSuper = false)
@Builder
@Entity
@Table(name = "vendor_status")
public class VendorStatus {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "VENDOR_ID", unique = true, nullable = false, updatable = false)
    private User vendor;
    @Column(name = "STATUS")
    @Enumerated(EnumType.STRING)
    private VendorVerificationStatus status;

    public VendorStatus(User vendor, VendorVerificationStatus status) {
        this.vendor = vendor;
        this.status = status;
    }
}


