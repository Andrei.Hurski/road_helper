package com.capstoneproject.road_helper.service;

import com.capstoneproject.road_helper.domain.Incident;
import com.capstoneproject.road_helper.domain.Proposal;
import com.capstoneproject.road_helper.dto.ProposePriceDto;

import java.util.List;
import java.util.Optional;

public interface ProposalService {

    Proposal save(Proposal proposal);

    Optional<Proposal> getById(Long id);

    List<Proposal> getByIncident(Incident incident);

    void createProposal(ProposePriceDto price, Incident incident);
}
