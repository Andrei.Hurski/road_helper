package com.capstoneproject.road_helper.service.impl;

import com.capstoneproject.road_helper.domain.CarModel;
import com.capstoneproject.road_helper.repository.CarModelRepository;
import com.capstoneproject.road_helper.service.CarModelService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CarModelServiceImpl implements CarModelService {

    private final CarModelRepository repository;

    public List<CarModel> findAll() {
        return repository.findAll(Sort.by("modelName"));
    }

    public CarModel save(CarModel model) {
        return repository.save(model);
    }
}
