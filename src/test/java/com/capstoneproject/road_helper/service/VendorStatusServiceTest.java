package com.capstoneproject.road_helper.service;

import com.capstoneproject.road_helper.domain.*;
import com.capstoneproject.road_helper.repository.VendorStatusRepository;
import com.capstoneproject.road_helper.service.impl.UserServiceImpl;
import com.capstoneproject.road_helper.service.impl.VendorStatusServiceImpl;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
class VendorStatusServiceTest {
    @InjectMocks
    private VendorStatusServiceImpl vendorStatusService;
    @Mock
    private UserServiceImpl userService;
    @Mock
    private VendorStatusRepository repository;
    private static final User VENDOR = new User(1L, "John", "john@email.com", "johnPassword", "+48777888999", new Role(1, RoleName.VENDOR));
    private static final VendorStatus STATUS = new VendorStatus(1L, VENDOR, VendorVerificationStatus.VERIFY);

    @Test
    void getByVendor_shouldReturnVendorStatus() {
        when(repository.getByVendor(VENDOR)).thenReturn(Optional.of(STATUS));
        VendorStatus actual = vendorStatusService.getByVendor(VENDOR);
        Assert.assertEquals(STATUS, actual);
    }

    @Test
    void getByVendor_shouldCallRepository() {
        vendorStatusService.getByVendor(VENDOR);
        verify(repository).getByVendor(VENDOR);
    }

    @Test
    void getVendorsByVerificationStatus_shouldReturnVendorStatus() {
        when(repository.getByStatus(VendorVerificationStatus.VERIFY)).thenReturn(List.of(STATUS));
        List<User> vendors = vendorStatusService.getVendorsByVerificationStatus(VendorVerificationStatus.VERIFY);
        Assert.assertEquals(VENDOR, vendors.get(0));
    }

    @Test
    void getVendorsByVerificationStatus_shouldCallRepository() {
        vendorStatusService.getVendorsByVerificationStatus(VendorVerificationStatus.VERIFY);
        verify(repository).getByStatus(VendorVerificationStatus.VERIFY);
    }

    @Test
    void save_shouldCallRepository() {
        vendorStatusService.save(STATUS);
        verify(repository).save(STATUS);
    }

//    @Test
//    void verifyVendor_shouldCallRepository() {
//        when(userService.findById(1L)).thenReturn(VENDOR);
//        when(repository.getByVendor(VENDOR)).thenReturn(STATUS);
//        vendorStatusService.verifyVendor(1L);
//        verify(repository).save(STATUS);
//    }

//    @Test
//    void cancelVendor_shouldCallRepository() {
//        when(userService.findById(1L)).thenReturn(VENDOR);
//        when(repository.getByVendor(VENDOR)).thenReturn(STATUS);
//        vendorStatusService.cancelVendor(1L);
//        verify(repository).save(STATUS);
//    }
}