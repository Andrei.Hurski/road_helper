package com.capstoneproject.road_helper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication

public class RoadHelperApplication {

    public static void main(String[] args) {
        SpringApplication.run(RoadHelperApplication.class, args);
    }

}
