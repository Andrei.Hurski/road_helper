package com.capstoneproject.road_helper.service.impl;

import com.capstoneproject.road_helper.domain.User;
import com.capstoneproject.road_helper.domain.VendorStatus;
import com.capstoneproject.road_helper.domain.VendorVerificationStatus;
import com.capstoneproject.road_helper.exception.message.ExceptionMessage;
import com.capstoneproject.road_helper.exception.VendorStatusNotFoundException;
import com.capstoneproject.road_helper.repository.VendorStatusRepository;
import com.capstoneproject.road_helper.service.VendorStatusService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Service
public class VendorStatusServiceImpl implements VendorStatusService {

    private final VendorStatusRepository repository;
    private final EmailSenderService emailService; // TODO emailService not use because for not flooding in email

    public VendorStatus getByVendor(User user) {
        return repository.getByVendor(user).orElseThrow(() ->
                new VendorStatusNotFoundException(ExceptionMessage.VENDOR_STATUS_NOT_FOUND));
    }

    public List<User> getVendorsByVerificationStatus(VendorVerificationStatus status) {
        List<User> userList = new ArrayList<>();
        List<VendorStatus> vendorStatusList = repository.getByStatus(status);
        for (VendorStatus vendorStatus : vendorStatusList) {
            userList.add(vendorStatus.getVendor());
        }
        return userList;
    }

    public void save(VendorStatus status) {
        repository.save(status);
    }

    public void verifyVendor(User vendor) {
        VendorStatus status = getByVendor(vendor);
        status.setStatus(VendorVerificationStatus.VERIFY);
        repository.save(status);
//        emailService.sendVendorVerifyStatusMail(vendor);
    }

    public void cancelVendor(User vendor) {
        VendorStatus status = getByVendor(vendor);
        status.setStatus(VendorVerificationStatus.CANCELED);
        repository.save(status);
//        emailService.sendVendorCancelStatusMail(vendor);
    }
}
