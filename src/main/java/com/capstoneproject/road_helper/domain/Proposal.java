package com.capstoneproject.road_helper.domain;


import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = {"id"})
@EqualsAndHashCode(exclude = {"id"}, callSuper = false)
@Builder
@Entity
@Table(name = "PROPOSALS")
public class Proposal {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "VENDOR_ID", unique = true, nullable = false, updatable = false)
    private User vendor;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "INCIDENT_ID", unique = true, nullable = false, updatable = false)
    private Incident incident;
    @Column(name = "PRICE")
    private BigDecimal price;
}
