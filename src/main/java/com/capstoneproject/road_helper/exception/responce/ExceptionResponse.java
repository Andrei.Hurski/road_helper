package com.capstoneproject.road_helper.exception.responce;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ExceptionResponse {

    private String status;
    private String message;
}
