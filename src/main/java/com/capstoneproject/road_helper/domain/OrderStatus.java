package com.capstoneproject.road_helper.domain;

public enum OrderStatus {
    COMPLETED, IN_PROCESS
}
