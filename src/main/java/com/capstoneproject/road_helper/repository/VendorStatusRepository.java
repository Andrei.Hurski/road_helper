package com.capstoneproject.road_helper.repository;

import com.capstoneproject.road_helper.domain.User;
import com.capstoneproject.road_helper.domain.VendorStatus;
import com.capstoneproject.road_helper.domain.VendorVerificationStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface VendorStatusRepository extends JpaRepository<VendorStatus, Long> {

    List<VendorStatus> getByStatus(VendorVerificationStatus status);

    Optional<VendorStatus> getByVendor(User vendor);
}
