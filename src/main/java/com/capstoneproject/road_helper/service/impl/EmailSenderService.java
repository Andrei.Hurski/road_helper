package com.capstoneproject.road_helper.service.impl;

import com.capstoneproject.road_helper.domain.Proposal;
import com.capstoneproject.road_helper.domain.User;
import com.capstoneproject.road_helper.service.message.MessageConst;
import lombok.RequiredArgsConstructor;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class EmailSenderService {

    private final JavaMailSender emailSender;

    private void sendSimpleMessage(String to, String subject, String text) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("help.on.road.service@gmail.com");
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        emailSender.send(message);
    }

    public void sendMailToVendorAboutClient(Proposal proposal) {
        sendSimpleMessage(
                proposal.getVendor().getEmail(),
                MessageConst.SUBJECT_MESSAGE_ACCEPT_PROPOSAL,
                String.format(MessageConst.MESSAGE_TO_VENDOR,
                        proposal.getIncident().getUser().getPhone(),
                        proposal.getIncident().getUser().getName()));
    }

    public void sendGreetingMailForUser(User user) {
        sendSimpleMessage(
                user.getEmail(),
                MessageConst.SUBJECT_MESSAGE_FIRST_MAIL,
                MessageConst.GREETING_MAIL_FOR_USER);
    }

    public void sendGreetingMailForVendor(User vendor) {
        sendSimpleMessage(
                vendor.getEmail(),
                MessageConst.SUBJECT_MESSAGE_FIRST_MAIL,
                MessageConst.GREETING_MAIL_FOR_VENDOR);
    }

    public void sendUserPassword(User user) {
        sendSimpleMessage(
                user.getEmail(),
                MessageConst.SUBJECT_MESSAGE_SEND_PASSWORD,
                String.format(MessageConst.SEND_PASSWORD,
                        user.getPassword()));
    }

    public void sendVendorVerifyStatusMail(User vendor) {
        sendSimpleMessage(
                vendor.getEmail(),
                MessageConst.SUBJECT_MESSAGE_VENDOR_STATUS,
                MessageConst.STATUS_MESSAGE_VERIFY);
    }

    public void sendVendorCancelStatusMail(User vendor) {
        sendSimpleMessage(
                vendor.getEmail(),
                MessageConst.SUBJECT_MESSAGE_VENDOR_STATUS,
                MessageConst.STATUS_MESSAGE_CANCEL);
    }
}
