package com.capstoneproject.road_helper.service;

import com.capstoneproject.road_helper.domain.Role;
import com.capstoneproject.road_helper.domain.RoleName;
import com.capstoneproject.road_helper.repository.RoleRepository;
import com.capstoneproject.road_helper.service.impl.RoleServiceImpl;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
class RoleServiceTest {
    @InjectMocks
    private RoleServiceImpl roleService;
    @Mock
    private RoleRepository repository;
    private static final Role USER_ROLE = new Role(1, RoleName.USER);

    @Test
    void getByRoleName_shouldReturnRole() {
        when(repository.getByRoleName(RoleName.USER)).thenReturn(USER_ROLE);
        Role actual = roleService.getByRoleName(RoleName.USER);
        Assert.assertEquals(USER_ROLE, actual);
    }

    @Test
    void getByRoleName_shouldCallRepository() {
        roleService.getByRoleName(RoleName.USER);
        verify(repository).getByRoleName(RoleName.USER);
    }
}