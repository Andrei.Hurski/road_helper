package com.capstoneproject.road_helper.domain;

public enum RoleName {
    USER, VENDOR, SERVICE
}
