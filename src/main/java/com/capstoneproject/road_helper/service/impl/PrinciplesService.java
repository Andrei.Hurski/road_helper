package com.capstoneproject.road_helper.service.impl;

import com.capstoneproject.road_helper.domain.User;
import com.capstoneproject.road_helper.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Optional;

@RequiredArgsConstructor
@Service
public class PrinciplesService {

    private final UserRepository userRepository;

    public Optional<User>  getPrinciples() {
        Optional<User> userOptional = Optional.empty();
        if (SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof UserDetails) {
            userOptional = userRepository.findByEmail(((UserDetails) SecurityContextHolder
                    .getContext()
                    .getAuthentication()
                    .getPrincipal())
                    .getUsername());
        }
        return userOptional;
    }
}
