package com.capstoneproject.road_helper.repository;

import com.capstoneproject.road_helper.domain.Incident;
import com.capstoneproject.road_helper.domain.Proposal;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProposalRepository extends JpaRepository<Proposal, Long> {

    List<Proposal> findAllByIncident(Incident incident);
}
