package com.capstoneproject.road_helper.service.impl;

import com.capstoneproject.road_helper.domain.RoleName;
import com.capstoneproject.road_helper.domain.VendorVerificationStatus;
import com.capstoneproject.road_helper.dto.CarDto;
import com.capstoneproject.road_helper.service.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

@Service
@RequiredArgsConstructor
public class AdminServiceImpl implements AdminService {

    private final UserService userService;
    private final OrderService orderService;
    private final IncidentService incidentService;
    private final RoleService roleService;
    private final CarService carService;
    private final VendorStatusService vendorStatusService;

    @Override
    public String getService(Model model) {
        model.addAttribute("vendors", userService.findAllByRole(roleService.getByRoleName(RoleName.VENDOR)));
        model.addAttribute("users", userService.findAllByRole(roleService.getByRoleName(RoleName.USER)));
        model.addAttribute("orders", orderService.findAll());
        model.addAttribute("incidents", incidentService.findAll());
        return "service";
    }

    @Override
    public String newCar(Model model) {
        model.addAttribute("car", new CarDto());
        return "newCar";
    }

    @Override
    public String newCar(CarDto carDto, BindingResult result) {
        if (result.hasErrors()) {
            return "newCar";
        } else {
            carService.save(carDto);
            return "redirect:/service";
        }
    }
    @Override
    public String getVendorsOnVerification(Model model) {
        model.addAttribute("vendors", vendorStatusService
                .getVendorsByVerificationStatus(VendorVerificationStatus.ON_VERIFICATION));
        return "vendors";
    }

    @Override
    public String verifyVendor(Long id) {
        vendorStatusService.verifyVendor(userService.findById(id));
        return "redirect:/vendors_on_verify";
    }

    @Override
    public String cancelVendor(Long id) {
        vendorStatusService.cancelVendor(userService.findById(id));
        return "redirect:/vendors_on_verify";
    }
}
