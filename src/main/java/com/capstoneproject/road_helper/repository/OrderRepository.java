package com.capstoneproject.road_helper.repository;

import com.capstoneproject.road_helper.domain.Incident;
import com.capstoneproject.road_helper.domain.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

    Order getByIncident(Incident incident);
}
