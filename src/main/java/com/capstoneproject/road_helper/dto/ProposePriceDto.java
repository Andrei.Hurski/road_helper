package com.capstoneproject.road_helper.dto;

import lombok.*;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Getter
@Setter
public class ProposePriceDto {
    @NotNull(message = "Price cannot be empty")
    private BigDecimal price;

    public ProposePriceDto(BigDecimal price) {
        this.price = price;
    }

    public ProposePriceDto() {
    }
}
