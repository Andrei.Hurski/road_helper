package com.capstoneproject.road_helper.service;

import com.capstoneproject.road_helper.domain.CarModel;

import java.util.List;

public interface CarModelService {

    List<CarModel> findAll();

    CarModel save(CarModel model);
}
