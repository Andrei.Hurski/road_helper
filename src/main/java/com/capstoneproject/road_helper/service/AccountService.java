package com.capstoneproject.road_helper.service;

import com.capstoneproject.road_helper.domain.User;
import com.capstoneproject.road_helper.dto.UpdateUserDto;
import org.springframework.ui.Model;

public interface AccountService {

    public String account(Model model);
}
