package com.capstoneproject.road_helper.exception;

import com.capstoneproject.road_helper.exception.responce.ExceptionResponse;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ControllerExceptionHandler {

    @ExceptionHandler(value = UserNotFoundException.class)
    String handleUserNotFoundException(UserNotFoundException e, Model model) {
        model.addAttribute("response", mapToResponse(e, HttpStatus.NOT_FOUND));
        return "error";
    }

    @ExceptionHandler(value = OrderNotFoundException.class)
    String handleOrderNotFoundException(OrderNotFoundException e, Model model) {
        model.addAttribute("response", mapToResponse(e, HttpStatus.NOT_FOUND));
        return "error";
    }

    @ExceptionHandler(value = IncidentNotFoundException.class)
    String handleIncidentNotFoundException(IncidentNotFoundException e, Model model) {
        model.addAttribute("response", mapToResponse(e, HttpStatus.NOT_FOUND));
        return "error";
    }

    private ExceptionResponse mapToResponse(RuntimeException e, HttpStatus status) {
        return ExceptionResponse.builder()
                .message(e.getMessage())
                .status(String.valueOf(status.value()))
                .build();
    }
}
