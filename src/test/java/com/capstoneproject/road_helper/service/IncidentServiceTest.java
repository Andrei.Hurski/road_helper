package com.capstoneproject.road_helper.service;

import com.capstoneproject.road_helper.domain.Incident;
import com.capstoneproject.road_helper.domain.User;
import com.capstoneproject.road_helper.repository.IncidentRepository;
import com.capstoneproject.road_helper.service.impl.IncidentServiceImpl;
import com.capstoneproject.road_helper.service.impl.PrinciplesService;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
class IncidentServiceTest {

    @InjectMocks
    private IncidentServiceImpl incidentService;
    @Mock
    private IncidentRepository incidentRepository;
    @Mock
    private PrinciplesService principlesService;

    private final List<Incident> USER_LIST_INCIDENTS = List.of(getIncident());

    @Test
    void saveIncident_shouldReturnIncident() {
        Incident incident = mock(Incident.class);
        when(incidentRepository.save(incident)).thenReturn(getIncident());
        Incident actual = incidentService.save(incident);
        Assert.assertEquals(getIncident(), actual);
    }

    @Test
    void findById_shouldReturnOptionalOfIncident() {
        when(incidentRepository.findById(getIncident().getId())).thenReturn(Optional.of(getIncident()));
        Optional<Incident> actual = incidentService.findById(getIncident().getId());
        Assert.assertEquals(Optional.of(getIncident()), actual);
    }

    @Test
    void getByUser() {
        User user = mock(User.class);
        when(incidentRepository.getByUser(user)).thenReturn(USER_LIST_INCIDENTS);
        List<Incident> actual = incidentService.getByUser(user);
        Assert.assertEquals(USER_LIST_INCIDENTS, actual);
    }

    @Test
    void getByUserAndInOrder() {
        User user = mock(User.class);
        when(principlesService.getPrinciples()).thenReturn(Optional.of(user));
        when(incidentRepository.findAllByUserAndInOrder(user, false)).thenReturn(USER_LIST_INCIDENTS);
        when(incidentRepository.findAllByUserAndInOrder(user, true)).thenReturn(List.of());
        Assert.assertEquals(USER_LIST_INCIDENTS, incidentService.getByUserInSessionAndInOrder(false));
        Assert.assertEquals(List.of(), incidentService.getByUserInSessionAndInOrder(true));
    }

    @Test
    void findAll() {
        when(incidentRepository.findAll()).thenReturn(USER_LIST_INCIDENTS);
        Assert.assertEquals(USER_LIST_INCIDENTS, incidentService.findAll());
    }

    private Incident getIncident() {
        return Incident.builder()
                .id(1L)
                .user(null)
                .description(null)
                .carBrand(null)
                .carModel(null)
                .carWeight(null)
                .locFrom(null)
                .locTo(null)
                .inOrder(false)
                .build();
    }
}