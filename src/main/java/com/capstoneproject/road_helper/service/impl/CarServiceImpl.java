package com.capstoneproject.road_helper.service.impl;

import com.capstoneproject.road_helper.domain.CarBrand;
import com.capstoneproject.road_helper.domain.CarModel;
import com.capstoneproject.road_helper.dto.CarDto;
import com.capstoneproject.road_helper.service.CarBrandService;
import com.capstoneproject.road_helper.service.CarModelService;
import com.capstoneproject.road_helper.service.CarService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CarServiceImpl implements CarService {

    private final CarModelService carModelService;
    private final CarBrandService carBrandServiceImpl;

    @Override
    public void save(CarDto carDto) {
        CarBrand carBrand = carBrandServiceImpl.save(CarBrand.builder().
                brandName(carDto.getCarBrand())
                .build());
        carModelService.save(CarModel.builder()
                .carBrand(carBrand)
                .modelName(carDto.getCarModel())
                .build());
    }
}
