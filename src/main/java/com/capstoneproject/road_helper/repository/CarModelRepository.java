package com.capstoneproject.road_helper.repository;

import com.capstoneproject.road_helper.domain.CarModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarModelRepository extends JpaRepository<CarModel, Long> {

}
