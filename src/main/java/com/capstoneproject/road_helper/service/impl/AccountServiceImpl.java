package com.capstoneproject.road_helper.service.impl;

import com.capstoneproject.road_helper.domain.User;
import com.capstoneproject.road_helper.dto.UpdateUserDto;
import com.capstoneproject.road_helper.exception.UserNotFoundException;
import com.capstoneproject.road_helper.exception.message.ExceptionMessage;
import com.capstoneproject.road_helper.service.AccountService;
import com.capstoneproject.road_helper.service.IncidentService;
import com.capstoneproject.road_helper.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

@Service
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService {

    private final PrinciplesService principlesService;
    private final OrderService orderService;
    private final IncidentService incidentService;

    @Override
    public String account(Model model) {
        User user = principlesService.getPrinciples().orElseThrow(() ->
                new UserNotFoundException(ExceptionMessage.USER_NOT_FOUND));
        model.addAttribute("orders", orderService.getOrdersByCurrentUser());
        model.addAttribute("incidents", incidentService.getByUser(user));
        model.addAttribute("user", user);
        model.addAttribute("userDto", new UpdateUserDto());
        return "account";
    }
}
