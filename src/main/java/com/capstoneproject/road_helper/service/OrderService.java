package com.capstoneproject.road_helper.service;

import com.capstoneproject.road_helper.domain.Order;
import org.springframework.ui.Model;

import java.util.List;

public interface OrderService {

    String createOrder(Long id);

    String getOrdersByUser(Model model);

    String completeOrder(Long id);

    List<Order> getOrdersByCurrentUser();

    List<Order> findAll();
}
