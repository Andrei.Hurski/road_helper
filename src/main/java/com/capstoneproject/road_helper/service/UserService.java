package com.capstoneproject.road_helper.service;

import com.capstoneproject.road_helper.domain.Role;
import com.capstoneproject.road_helper.domain.User;
import com.capstoneproject.road_helper.dto.AuthUserDto;
import com.capstoneproject.road_helper.dto.RegUserDto;
import com.capstoneproject.road_helper.dto.UpdateUserDto;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import java.util.List;
import java.util.Optional;

public interface UserService {

    User save(RegUserDto userDto);

    User findById(long id);

    Optional<User> findByEmail(String email);

    List<User> findAllByRole(Role role);

    String getRegForm(Model model);

    String save(RegUserDto userDto, BindingResult result, Model model);

    String getAuthForm(String error, Model model);

    String getPassword(Model model);

    String sendPassword(AuthUserDto userDto, BindingResult result, Model model);

    String getInfo(Long id, Model model);

    String update(Long id, UpdateUserDto userDto);
}
