package com.capstoneproject.road_helper.service;

import com.capstoneproject.road_helper.dto.CarDto;

public interface CarService {

    void save(CarDto carDto);
}
