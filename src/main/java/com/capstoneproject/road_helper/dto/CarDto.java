package com.capstoneproject.road_helper.dto;

import lombok.*;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CarDto {
    @NotEmpty(message = "Brand cannot be empty.")
    private String carBrand;
    @NotEmpty(message = "Model cannot be empty.")
    private String carModel;
}
