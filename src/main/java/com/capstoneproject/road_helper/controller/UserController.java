package com.capstoneproject.road_helper.controller;

import com.capstoneproject.road_helper.dto.AuthUserDto;
import com.capstoneproject.road_helper.dto.RegUserDto;
import com.capstoneproject.road_helper.dto.UpdateUserDto;
import com.capstoneproject.road_helper.service.AccountService;
import com.capstoneproject.road_helper.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;
    private final AccountService accountService;

    @GetMapping("/registration")
    public String getRegForm(Model model) {
        return userService.getRegForm(model);
    }

    @PostMapping("/registration")
    public String registration(@Valid @ModelAttribute("user") RegUserDto userDto, BindingResult result, Model model) {
        return userService.save(userDto, result, model);
    }


    @GetMapping("/login")
    public String getAuthForm(@RequestParam(value = "error", required = false) String error, Model model) {
        return userService.getAuthForm(error, model);
    }

    @GetMapping("/password")
    public String getPassword(Model model) {
        return userService.getPassword(model);
    }

    @PostMapping("/password")
    public String sendPassword(@Valid @ModelAttribute("user") AuthUserDto userDto, BindingResult result, Model model) {
        return userService.sendPassword(userDto, result, model);
    }

    @GetMapping("/user/{id}")
    public String getInfo(@PathVariable Long id, Model model) {
        return userService.getInfo(id, model);
    }

    @PostMapping("/user/{id}")
    public String update(@PathVariable Long id, @ModelAttribute("userDto") UpdateUserDto userDto) {
        return userService.update(id, userDto);
    }

    @GetMapping("/user/account")
    public String account(Model model) {
        return accountService.account(model);
    }
}