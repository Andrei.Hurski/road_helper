package com.capstoneproject.road_helper.domain;

public enum VendorVerificationStatus {
    ON_VERIFICATION, VERIFY, CANCELED
}
