package com.capstoneproject.road_helper.exception;

public class VendorStatusNotFoundException extends RuntimeException {

    public VendorStatusNotFoundException(String message) {
        super(message);
    }
}
