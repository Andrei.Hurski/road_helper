package com.capstoneproject.road_helper.service.message;

public class MessageConst {
    public static final String MESSAGE_TO_VENDOR = "Your proposal is accept. Client is waiting for you!\nClient contact: %s %s";
    public static final String SUBJECT_MESSAGE_ACCEPT_PROPOSAL = "Your proposal is accept";
    public static final String SUBJECT_MESSAGE_FIRST_MAIL = "First mail";
    public static final String GREETING_MAIL_FOR_USER = "Thank you for using our service!";
    public static final String GREETING_MAIL_FOR_VENDOR = "Thank you for using our service! \n After verification," +
            " you can find clients in our service";
    public static final String SUBJECT_MESSAGE_SEND_PASSWORD = "Your password for road-help service";
    public static final String SEND_PASSWORD = "Password : %s";
    public static final String SUBJECT_MESSAGE_VENDOR_STATUS = "Your status";
    public static final String STATUS_MESSAGE_VERIFY = "Your status is VERIFY";
    public static final String STATUS_MESSAGE_CANCEL = "Your status is CANCEL";
}
