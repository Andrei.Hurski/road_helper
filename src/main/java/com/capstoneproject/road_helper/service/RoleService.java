package com.capstoneproject.road_helper.service;

import com.capstoneproject.road_helper.domain.Role;
import com.capstoneproject.road_helper.domain.RoleName;

public interface RoleService {

    public Role getByRoleName(RoleName role);
}
