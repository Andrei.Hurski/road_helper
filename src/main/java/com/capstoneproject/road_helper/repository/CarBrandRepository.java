package com.capstoneproject.road_helper.repository;

import com.capstoneproject.road_helper.domain.CarBrand;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarBrandRepository extends JpaRepository<CarBrand, Long> {

}
