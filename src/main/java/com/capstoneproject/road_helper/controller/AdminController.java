package com.capstoneproject.road_helper.controller;

import com.capstoneproject.road_helper.dto.CarDto;
import com.capstoneproject.road_helper.service.AdminService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequiredArgsConstructor
@RequestMapping("/admin")
public class AdminController {

    private final AdminService adminService;

    @GetMapping
    public String getService(Model model) {
        return adminService.getService(model);
    }

    @GetMapping("/new_car")
    public String newCar(Model model) {
        return adminService.newCar(model);
    }

    @PostMapping("/new_car")
    public String newCar(@Valid @ModelAttribute("car") CarDto carDto, BindingResult result) {
        return adminService.newCar(carDto, result);
    }

    @GetMapping("/vendors")
    public String getVendorsOnVerification(Model model) {
        return adminService.getVendorsOnVerification(model);
    }

    @PostMapping("/verify/{id}")
    public String verifyVendor(@PathVariable Long id) {
        return adminService.verifyVendor(id);
    }

    @PostMapping("/cancel/{id}")
    public String cancelVendor(@PathVariable Long id) {
        return adminService.cancelVendor(id);
    }
}
