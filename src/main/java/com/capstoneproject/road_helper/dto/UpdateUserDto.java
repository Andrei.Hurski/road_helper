package com.capstoneproject.road_helper.dto;


import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UpdateUserDto {
    private String name;
    private String password;
    private String phone;
}
