package com.capstoneproject.road_helper.dto;

import lombok.*;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class IncidentDto {
    @NotEmpty(message = "Description cannot be empty.")
    private String description;
    private String carBrand;
    private String carModel;
    private Integer carWeight;
    private String locFrom;
    private String locTo;
}
