package com.capstoneproject.road_helper.service.impl;

import com.capstoneproject.road_helper.domain.*;
import com.capstoneproject.road_helper.dto.AuthUserDto;
import com.capstoneproject.road_helper.dto.RegUserDto;
import com.capstoneproject.road_helper.dto.UpdateUserDto;
import com.capstoneproject.road_helper.repository.UserRepository;
import com.capstoneproject.road_helper.service.RoleService;
import com.capstoneproject.road_helper.service.UserService;
import com.capstoneproject.road_helper.service.VendorStatusService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository repository;
    private final VendorStatusService vendorStatusService;
    private final RoleService roleService;
    private final PrinciplesService principlesService;
    private final EmailSenderService emailService; // TODO emailService not use because for not flooding in email

    private static final String NOT_FOUND_USER_BY_EMAIL = "Not found user with %s email!";
    private static final String USER_ALREADY_CREATED = "User with %s email already exists";

    @Override
    public User save(RegUserDto userDto) {
        User user = mapToUser(userDto);
        User saveUser = save(user);
        if (user.getRole().getRoleName().equals(RoleName.VENDOR)) {
            VendorStatus vendorStatus = VendorStatus.builder()
                    .vendor(saveUser)
                    .status(VendorVerificationStatus.ON_VERIFICATION)
                    .build();
            vendorStatusService.save(vendorStatus);
//          emailSenderService.sendGreetingMailForVendor(user);
        } else {
//          emailSenderService.sendGreetingMailForUser(user);
        }
        return save(user);
    }

    @Override
    public User findById(long id) {
        return repository.findById(id).orElseThrow(() -> new RuntimeException("User not found"));
    }

    @Override
    public Optional<User> findByEmail(String email) {
        return repository.findByEmail(email);
    }

    @Override
    public List<User> findAllByRole(Role role) {
        return repository.findAllByRole(role);
    }

    @Override
    public String getRegForm(Model model) {
        model.addAttribute("user", new RegUserDto());
        return "registration";
    }

    @Override
    public String save(RegUserDto userDto, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "registration";
        }
        if (findByEmail(userDto.getEmail()).isEmpty()) {
            save(userDto);
            return "redirect:/";
        }
        model.addAttribute("emailExist", String.format(USER_ALREADY_CREATED, userDto.getEmail()));
        return "registration";
    }

    @Override
    public String getAuthForm(String error, Model model) {
        if (error != null) {
            model.addAttribute("error", error);
        }
        return "login";
    }

    @Override
    public String getPassword(Model model) {
        model.addAttribute("user", new AuthUserDto());
        return "rememberPass";
    }

    @Override
    public String sendPassword(AuthUserDto userDto, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "rememberPass";
        }
        Optional<User> optionalUser = findByEmail(userDto.getEmail());
        if (optionalUser.isPresent()) {
//            emailService.sendUserPassword(optionalUser.get());
        } else {
            model.addAttribute("message", String.format(NOT_FOUND_USER_BY_EMAIL, userDto.getEmail()));
            return "rememberPass";
        }
        return "redirect:/";
    }

    @Override
    public String getInfo(Long id, Model model) {
        model.addAttribute("user", findById(id));
        model.addAttribute("userDto", new UpdateUserDto());
        return "user_info";
    }

    @Override
    public String update(Long id, UpdateUserDto userDto) {
        update(userDto, findById(id));
        Optional<User> user = principlesService.getPrinciples();
        if (user.isPresent() && user.get().getRole().getRoleName().equals(RoleName.SERVICE)) {
            return "redirect:/user/{id}";
        }
        return "redirect:/user/account";
    }

    private User save(User user) {
        return repository.save(user);
    }

    private void update(UpdateUserDto userDto, User user) {
        if (userDto.getName() != null) {
            user.setName(userDto.getName());
        }
        if (userDto.getPassword() != null) {
            user.setPassword(userDto.getPassword());
        }
        if (userDto.getPhone() != null) {
            user.setPhone(userDto.getPhone());
        }
        save(user);
    }

    private User mapToUser(RegUserDto userDto) {
        return User.builder()
                .name(userDto.getName())
                .email(userDto.getEmail())
                .password(userDto.getPassword())
                .phone(userDto.getPhone())
                .role(roleService.getByRoleName(userDto.getRole()))
                .build();
    }
}
