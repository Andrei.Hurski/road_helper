package com.capstoneproject.road_helper.domain;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = {"id"})
@EqualsAndHashCode(exclude = {"id"}, callSuper = false)
@Builder
@Entity
@Table(name = "incidents")
public class Incident {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "USER_ID", unique = true, nullable = false, updatable = false)
    private User user;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "CAR_BRAND")
    private String carBrand;
    @Column(name = "CAR_MODEL")
    private String carModel;
    @Column(name = "CAR_WEIGHT")
    private Integer carWeight;
    @Column(name = "LOC_FROM")
    private String locFrom;
    @Column(name = "LOC_TO")
    private String locTo;
    @Column(name = "IN_ORDER")
    private boolean inOrder;
}