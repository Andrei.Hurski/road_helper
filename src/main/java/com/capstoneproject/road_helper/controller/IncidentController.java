package com.capstoneproject.road_helper.controller;

import com.capstoneproject.road_helper.dto.IncidentDto;
import com.capstoneproject.road_helper.dto.ProposePriceDto;
import com.capstoneproject.road_helper.service.impl.IncidentServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequiredArgsConstructor
@RequestMapping("/incident")
public class IncidentController {

    private final IncidentServiceImpl incidentService;

    @GetMapping
    public String getForm(Model model) {
        return incidentService.getForm(model);
    }

    @PostMapping
    public String createIncident(@Valid @ModelAttribute("incident") IncidentDto incidentDto, BindingResult result) {
        return incidentService.createIncident(incidentDto, result);
    }

    @GetMapping("/all")
    public String getUserIncidents(Model model) {
        return incidentService.getUserIncidents(model);
    }

    @GetMapping("/{id}/proposals")
    public String getProposals(@RequestParam(value = "error", required = false) String error,
                               @PathVariable Long id, Model model) {
        return incidentService.getProposals(error, id, model);
    }

    @GetMapping("/{id}")
    public String getIncidentById(@PathVariable Long id, Model model) {
        return incidentService.getIncidentById(id, model);
    }

    @GetMapping("/no_order")
    public String getIncidentsWithoutVendor(@RequestParam(value = "error", required = false) String error,
                                            Model model) {
        return incidentService.getIncidentsWithoutVendor(error, model);
    }

    @PostMapping("/{id}/proposal")
    public String proposePrice(@PathVariable Long id,
                               @Valid @ModelAttribute("priceDto") ProposePriceDto price,
                               BindingResult result,
                               Model model) {
        return incidentService.proposePrice(id, price, result, model);
    }
}
