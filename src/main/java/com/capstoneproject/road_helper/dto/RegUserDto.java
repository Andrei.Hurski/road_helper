package com.capstoneproject.road_helper.dto;

import com.capstoneproject.road_helper.domain.RoleName;
import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = {"id"})
@EqualsAndHashCode(exclude = {"id"}, callSuper = false)
@Builder
public class RegUserDto {
    @NotEmpty(message = "Name cannot be empty.")
    private String name;
    @Email
    @NotEmpty(message = "Email cannot be empty.")
    private String email;
    @Size(min = 8, message = "Cannot be less than 8 symbols")
    private String password;
    @NotEmpty(message = "Phone cannot be empty.")
    private String phone;
    private RoleName role;
}
