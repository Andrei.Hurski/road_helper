package com.capstoneproject.road_helper.service.impl;

import com.capstoneproject.road_helper.domain.CarBrand;
import com.capstoneproject.road_helper.repository.CarBrandRepository;
import com.capstoneproject.road_helper.service.CarBrandService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CarBrandServiceImpl implements CarBrandService {
    private final CarBrandRepository repository;

    public List<CarBrand> findAll() {
        return repository.findAll(Sort.by("brandName"));
    }

    public CarBrand save(CarBrand brand) {
        return repository.save(brand);
    }
}
