package com.capstoneproject.road_helper.exception.message;

public class ExceptionMessage {

    public static final String USER_NOT_FOUND = "User not found";
    public static final String ORDER_NOT_FOUND = "Order not found";
    public static final String INCIDENT_NOT_FOUND = "Incident not found";
    public static final String VENDOR_STATUS_NOT_FOUND = "User is not a vendor";
}
