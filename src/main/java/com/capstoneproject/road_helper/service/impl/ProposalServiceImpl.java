package com.capstoneproject.road_helper.service.impl;

import com.capstoneproject.road_helper.domain.Incident;
import com.capstoneproject.road_helper.domain.Proposal;
import com.capstoneproject.road_helper.domain.User;
import com.capstoneproject.road_helper.dto.ProposePriceDto;
import com.capstoneproject.road_helper.repository.ProposalRepository;
import com.capstoneproject.road_helper.service.ProposalService;
import com.capstoneproject.road_helper.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class ProposalServiceImpl implements ProposalService {

    private final ProposalRepository repository;
    private final PrinciplesService principlesService;

    public Proposal save(Proposal proposal) {
        return repository.save(proposal);
    }

    public Optional<Proposal> getById(Long id) {
        return repository.findById(id);
    }

    public List<Proposal> getByIncident(Incident incident) {
        return repository.findAllByIncident(incident);
    }

    public void createProposal(ProposePriceDto price, Incident incident) {
        Optional<User> vendor = principlesService.getPrinciples();
        save(Proposal.builder()
                .vendor(vendor.orElseThrow(() -> new RuntimeException("User not found")))
                .incident(incident)
                .price(price.getPrice())
                .build());
    }
}

