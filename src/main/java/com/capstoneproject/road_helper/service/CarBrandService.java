package com.capstoneproject.road_helper.service;

import com.capstoneproject.road_helper.domain.CarBrand;

import java.util.List;

public interface CarBrandService {

    List<CarBrand> findAll();

    CarBrand save(CarBrand brand);
}
