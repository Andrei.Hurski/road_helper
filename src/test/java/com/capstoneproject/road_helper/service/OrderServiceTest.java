package com.capstoneproject.road_helper.service;

import com.capstoneproject.road_helper.domain.Incident;
import com.capstoneproject.road_helper.domain.Order;
import com.capstoneproject.road_helper.domain.User;
import com.capstoneproject.road_helper.repository.OrderRepository;
import com.capstoneproject.road_helper.service.impl.IncidentServiceImpl;
import com.capstoneproject.road_helper.service.impl.OrderServiceImpl;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
class OrderServiceTest {
    @InjectMocks
    private OrderServiceImpl orderService;
    @Mock
    private OrderRepository repository;
    @Mock
    private IncidentServiceImpl incidentService;
    private static final Order SAVE_ORDER = new Order(1L, null, null,
            null, null, null);

//    @Test
//    void saveOrder_shouldReturnOrderWithId() {
//        Order order = mock(Order.class);
//
//        when(repository.save(order)).thenReturn(SAVE_ORDER);
//        Order actual = orderService.save(order);
//        Assert.assertNotNull(actual);
//        Assert.assertEquals(SAVE_ORDER, actual);
//        verify(repository).save(order);
//    }

//    @Test
//    void findById_shouldReturnOrder() {
//        when(repository.getById(1L)).thenReturn(SAVE_ORDER);
//        Order actual = orderService.findById(1L);
//        Assert.assertNotNull(actual);
//        Assert.assertEquals(SAVE_ORDER, actual);
//        verify(repository).getById(1L);
//    }

    @Test
    void getOrdersByUser_shouldReturnOrderList() {
        User user = mock(User.class);
        Incident incident = mock(Incident.class);
        Order order = mock(Order.class);
        List<Incident> incidentList = new ArrayList<>();
        for (int i = 1; i < 10; i++) {
            incidentList.add(incident);
        }
        when(incidentService.getByUserInSessionAndInOrder(true)).thenReturn(incidentList);
        when(repository.getByIncident(incident)).thenReturn(order);
        List<Order> actual = orderService.getOrdersByCurrentUser();
        Assert.assertNotNull(actual);
        verify(repository, times(9)).getByIncident(incident);
    }

    @Test
    void findAll_shouldReturnOrderList() {
        when(repository.findAll()).thenReturn(List.of(SAVE_ORDER));
        List<Order> actual = orderService.findAll();
        Assert.assertNotNull(actual);
        Assert.assertEquals(List.of(SAVE_ORDER), actual);
        verify(repository).findAll();
    }
}