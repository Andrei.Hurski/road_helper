package com.capstoneproject.road_helper.service;

import com.capstoneproject.road_helper.domain.User;
import com.capstoneproject.road_helper.domain.VendorStatus;
import com.capstoneproject.road_helper.domain.VendorVerificationStatus;

import java.util.List;

public interface VendorStatusService {

    VendorStatus getByVendor(User user);

    List<User> getVendorsByVerificationStatus(VendorVerificationStatus status);

    void save(VendorStatus status);

    void verifyVendor(User vendor);

    void cancelVendor(User vendor);
}
