package com.capstoneproject.road_helper.controller;

import com.capstoneproject.road_helper.service.impl.IndexService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequiredArgsConstructor
@RequestMapping("/")
public class IndexController {

    private final IndexService indexService;

    @GetMapping
    public String index(Model model) {

        return indexService.getIndex(model);
    }
}
