package com.capstoneproject.road_helper.service.impl;

import com.capstoneproject.road_helper.domain.RoleName;
import com.capstoneproject.road_helper.domain.User;
import com.capstoneproject.road_helper.domain.VendorVerificationStatus;
import com.capstoneproject.road_helper.service.VendorStatusService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.util.Optional;

@RequiredArgsConstructor
@Service
public class IndexService {

    private final VendorStatusService statusService;
    private final PrinciplesService principlesService;

    public String getIndex(Model model) {
        Optional<User> user = principlesService.getPrinciples();
        if (user.isPresent() && user.get().getRole().getRoleName().equals(RoleName.VENDOR)) {
            VendorVerificationStatus status = statusService.getByVendor(user.get()).getStatus();
            model.addAttribute("vendorStatus", status.toString());
        }
        return "index";
    }
}

