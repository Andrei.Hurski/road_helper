package com.capstoneproject.road_helper.service;

import com.capstoneproject.road_helper.dto.CarDto;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

public interface AdminService {

    String getService(Model model);

    String newCar(Model model);

    String newCar(CarDto carDto, BindingResult result);

    String getVendorsOnVerification(Model model);

    String verifyVendor(Long id);

    String cancelVendor(Long id);
}
