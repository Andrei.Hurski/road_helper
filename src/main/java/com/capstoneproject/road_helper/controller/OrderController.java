package com.capstoneproject.road_helper.controller;

import com.capstoneproject.road_helper.service.impl.OrderServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequiredArgsConstructor
@RequestMapping("/order")
public class OrderController {

    private final OrderServiceImpl orderService;

    @PostMapping("/{id}")
    public String createOrder(@PathVariable Long id) {
        return orderService.createOrder(id);
    }

    @GetMapping("/orders")
    public String getOrdersByUser(Model model) {
        return orderService.getOrdersByUser(model);
    }

    @PostMapping("/complete/{id}")
    public String completeOrder(@PathVariable Long id) {
        return orderService.completeOrder(id);
    }
}
