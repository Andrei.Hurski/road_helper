package com.capstoneproject.road_helper.service.impl;

import com.capstoneproject.road_helper.domain.Incident;
import com.capstoneproject.road_helper.domain.Proposal;
import com.capstoneproject.road_helper.domain.User;
import com.capstoneproject.road_helper.dto.IncidentDto;
import com.capstoneproject.road_helper.dto.ProposePriceDto;
import com.capstoneproject.road_helper.exception.message.ExceptionMessage;
import com.capstoneproject.road_helper.exception.IncidentNotFoundException;
import com.capstoneproject.road_helper.exception.UserNotFoundException;
import com.capstoneproject.road_helper.repository.IncidentRepository;
import com.capstoneproject.road_helper.service.CarBrandService;
import com.capstoneproject.road_helper.service.CarModelService;
import com.capstoneproject.road_helper.service.IncidentService;
import com.capstoneproject.road_helper.service.ProposalService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class IncidentServiceImpl implements IncidentService {

    private final IncidentRepository repository;
    private final PrinciplesService principlesService;
    private final CarBrandService brandService;
    private final CarModelService modelService;
    private final ProposalService proposalService;

    @Override
    public String createIncident(IncidentDto incidentDto, BindingResult result) {
        if (result.hasErrors()) {
            return "incident";
        }
        Optional<User> user = principlesService.getPrinciples();
        Incident incident = mapToIncident(
                incidentDto,
                user.orElseThrow(() -> new UserNotFoundException(ExceptionMessage.USER_NOT_FOUND)));
        save(incident);
        return "redirect:/";
    }

    @Override
    public String getForm(Model model) {
        model.addAttribute("brands", brandService.findAll());
        model.addAttribute("models", modelService.findAll());
        model.addAttribute("incident", new IncidentDto());
        return "incident";
    }

    @Override
    public String getUserIncidents(Model model) {
        model.addAttribute("incidents", getByUserInSessionAndInOrder(false));
        return "my_incidents";
    }

    @Override
    public String getProposals(String error, Long id, Model model) {
        Optional<Incident> optionalIncident = findById(id);
        if (optionalIncident.isPresent()) {
            List<Proposal> proposals = proposalService.getByIncident(optionalIncident.get());
            model.addAttribute("proposals", proposals);
            if (error != null) {
                model.addAttribute("error", error);
            }
        } else {
            model.addAttribute("message", "No incident with id=" + id);
        }
        return "proposals";
    }

    @Override
    public String getIncidentById(Long id, Model model) {
        Optional<Incident> optionalIncident = findById(id);
        if (optionalIncident.isPresent()
                && getByInOrder(false).contains(optionalIncident.get())) {
            model.addAttribute("incident", optionalIncident.get());
            model.addAttribute("priceDto", new ProposePriceDto());
            return "incidentById";
        }
        throw new IncidentNotFoundException(ExceptionMessage.INCIDENT_NOT_FOUND);
    }

    @Override
    public String getIncidentsWithoutVendor(String error, Model model) {
        model.addAttribute("incidents", getByInOrder(false));
        if (error != null) {
            model.addAttribute("error", error);
        }
        return "search_order";
    }

    @Override
    public String proposePrice(Long id, ProposePriceDto price, BindingResult result, Model model) {
        Optional<Incident> optionalIncident = findById(id);
        if (result.hasErrors() && optionalIncident.isPresent()) {
            model.addAttribute("incident", optionalIncident.get());
            return "incidentById";
        }
        if (optionalIncident.isPresent()
                && getByInOrder(false).contains(optionalIncident.get())) {
            proposalService.createProposal(price, optionalIncident.get());
            return "redirect:/incident/no_order";
        }
        return "redirect:/incident/no_order?error=proposal";
    }

    @Override
    public List<Incident> getByUser(User user) {
        return repository.getByUser(user);
    }

    @Override
    public List<Incident> findAll() {
        return repository.findAll();
    }

    @Override
    public Incident save(Incident incident) {
        return repository.save(incident);
    }

    @Override
    public List<Incident> getByUserInSessionAndInOrder(Boolean inOrder) {
        Optional<User> user = principlesService.getPrinciples();
        return repository.findAllByUserAndInOrder(
                user.orElseThrow(() -> new UserNotFoundException(ExceptionMessage.USER_NOT_FOUND)),
                inOrder);
    }

    public Optional<Incident> findById(Long id) {
        return repository.findById(id);
    }

    private List<Incident> getByInOrder(boolean inOrder) {
        return repository.findAllByInOrder(inOrder);
    }

    private Incident mapToIncident(IncidentDto incidentDto, User user) {
        return Incident.builder()
                .user(user)
                .description(incidentDto.getDescription())
                .carBrand(incidentDto.getCarBrand())
                .carModel(incidentDto.getCarModel())
                .carWeight(incidentDto.getCarWeight())
                .locFrom(incidentDto.getLocFrom())
                .locTo(incidentDto.getLocTo())
                .inOrder(false)
                .build();
    }
}