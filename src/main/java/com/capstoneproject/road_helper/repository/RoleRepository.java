package com.capstoneproject.road_helper.repository;

import com.capstoneproject.road_helper.domain.Role;
import com.capstoneproject.road_helper.domain.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {

    Role getByRoleName(RoleName role);
}
