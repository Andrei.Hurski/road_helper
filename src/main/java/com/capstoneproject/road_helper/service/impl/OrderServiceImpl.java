package com.capstoneproject.road_helper.service.impl;

import com.capstoneproject.road_helper.domain.Incident;
import com.capstoneproject.road_helper.domain.Order;
import com.capstoneproject.road_helper.domain.OrderStatus;
import com.capstoneproject.road_helper.domain.Proposal;
import com.capstoneproject.road_helper.exception.message.ExceptionMessage;
import com.capstoneproject.road_helper.exception.IncidentNotFoundException;
import com.capstoneproject.road_helper.exception.OrderNotFoundException;
import com.capstoneproject.road_helper.repository.OrderRepository;
import com.capstoneproject.road_helper.service.IncidentService;
import com.capstoneproject.road_helper.service.OrderService;
import com.capstoneproject.road_helper.service.ProposalService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderRepository repository;
    private final IncidentService incidentService;
    private final ProposalService proposalService;
    private final EmailSenderService emailSenderService;

    @Override
    public String createOrder(Long id) {
        createOrderByProposalId(id);
//        emailSenderService.sendMailToVendorAboutClient(proposalService.getById(id));
        return "redirect:/";
    }

    @Override
    public String getOrdersByUser(Model model) {
        model.addAttribute("orders", getOrdersByCurrentUser());
        return "my_orders";
    }

    @Override
    public String completeOrder(Long id) {
        Optional<Order> optionalOrder = repository.findById(id);
        Order order = optionalOrder.orElseThrow(() -> new OrderNotFoundException(ExceptionMessage.ORDER_NOT_FOUND));
        order.setOrderStatus(OrderStatus.COMPLETED);
        repository.save(order);
        return "redirect:/user/account";
    }

    @Override
    public List<Order> getOrdersByCurrentUser() {
        List<Order> ordersByUser = new ArrayList<>();
        List<Incident> incidentList = incidentService.getByUserInSessionAndInOrder(true);
        for (Incident incident : incidentList) {
            ordersByUser.add(repository.getByIncident(incident));
        }
        return ordersByUser;
    }

    @Override
    public List<Order> findAll() {
        return repository.findAll();
    }

    private void createOrderByProposalId(Long proposalId) {
        Optional<Proposal> optionalProposal = proposalService.getById(proposalId);
        if (optionalProposal.isPresent()
                && !optionalProposal.get().getIncident().isInOrder()) {
            Proposal proposal = optionalProposal.get();
            proposal.getIncident().setInOrder(true);
            incidentService.save(proposal.getIncident());
//            String now = LocalDateTime.now().format(DateTimeFormatter.ofPattern("d::MMM::uuuu HH::mm"));
            LocalDateTime ldt = LocalDateTime.parse(LocalDateTime.now(), DateTimeFormatter.ofPattern("d::MMM::uuuu HH::mm"));
            Order order = Order.builder()
                    .date(ldt)
                    .vendor(proposal.getVendor())
                    .incident(proposal.getIncident())
                    .price(proposal.getPrice())
                    .orderStatus(OrderStatus.IN_PROCESS)
                    .build();
             repository.save(order);
        }
        else {
            throw new IncidentNotFoundException(ExceptionMessage.INCIDENT_NOT_FOUND);
        }
    }
}
