package com.capstoneproject.road_helper.service.impl;

import com.capstoneproject.road_helper.domain.Role;
import com.capstoneproject.road_helper.domain.RoleName;
import com.capstoneproject.road_helper.repository.RoleRepository;
import com.capstoneproject.road_helper.service.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {

    private final RoleRepository repository;

    public Role getByRoleName(RoleName role) {
        return repository.getByRoleName(role);
    }
}
