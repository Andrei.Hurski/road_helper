package com.capstoneproject.road_helper.domain;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = {"id"})
@EqualsAndHashCode(exclude = {"id"}, callSuper = false)
@Builder
@Entity
@Table(name = "CAR_MODELS")
public class CarModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "BRAND", unique = true, nullable = false, updatable = false)
    private CarBrand carBrand;
    @Column(name = "MODEL_NAME")
    private String modelName;
}
