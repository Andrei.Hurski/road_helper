package com.capstoneproject.road_helper.repository;

import com.capstoneproject.road_helper.domain.Role;
import com.capstoneproject.road_helper.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    List<User> findAllByRole(Role role);

    Optional<User> findByEmail(String email);
}
