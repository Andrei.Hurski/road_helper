package com.capstoneproject.road_helper.domain;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = {"id"})
@EqualsAndHashCode(exclude = {"id"}, callSuper = false)
@Builder
@Entity
@Table(name = "ROLES")
public class Role {
    @Id
    @Column(name = "ID")
    private int id;
    @Column(name = "ROLE_NAME")
    @Enumerated(EnumType.STRING)
    private RoleName roleName;
}
