package com.capstoneproject.road_helper.service;

import com.capstoneproject.road_helper.domain.Incident;
import com.capstoneproject.road_helper.domain.User;
import com.capstoneproject.road_helper.dto.IncidentDto;
import com.capstoneproject.road_helper.dto.ProposePriceDto;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import java.util.List;
import java.util.Optional;

public interface IncidentService {

    String createIncident(IncidentDto incidentDto, BindingResult result);

    String getForm(Model model);

    String getUserIncidents(Model model);

    String getProposals(String error, Long id, Model model);

    String getIncidentById(Long id, Model model);

    String getIncidentsWithoutVendor(String error, Model model);

    String proposePrice(Long id, ProposePriceDto price, BindingResult result, Model model);

    List<Incident> getByUser(User user);

    List<Incident> findAll();

    Incident save(Incident incident);

    List<Incident> getByUserInSessionAndInOrder(Boolean inOrder);

    Optional<Incident> findById(Long id);
}
